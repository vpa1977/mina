#!/bin/sh -e

# Make a DFSG version of upstream tarball
# - remove all packaged JAR
# - remove non-free VSD files (MS Visio)
# - remove generated Javadoc and Xref
# - remove LICENCE.*.txt from other projects

# $2 = version
# $3 = file
DIR=mina-$2.dfsg
TAR=mina_$2.dfsg.orig.tar.gz

# clean up the upstream tarball
tar zxf $3
mv mina-$2 $DIR
GZIP=--best tar czf $TAR -X debian/orig-tar.exclude $DIR
rm -rf $DIR

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
